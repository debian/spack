# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/spack/issues
# Bug-Submit: https://github.com/<user>/spack/issues/new
# Changelog: https://github.com/<user>/spack/blob/master/CHANGES
# Documentation: https://github.com/<user>/spack/wiki
# Repository-Browse: https://github.com/<user>/spack
# Repository: https://github.com/<user>/spack.git
